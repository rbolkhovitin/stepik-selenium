from selenium import webdriver
import time

try:
    browser = webdriver.Chrome()
    browser.maximize_window()
    browser.get("http://suninjuly.github.io/huge_form.html")
    print("page loaded")
    elements = browser.find_elements_by_tag_name("input")
    assert len(elements) == 100
    print("elements found")
    for element in elements:
       element.send_keys("Мой ответ")
    print("data sent")
    button = browser.find_element_by_css_selector("button.btn")
    button.click()

finally:
    time.sleep(30)
    browser.quit()

