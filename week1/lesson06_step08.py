from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
import time 


def get_alert_text():
    try:
        WebDriverWait(browser, 3).until(
                EC.alert_is_present(),
                'Timed out waiting for PA creation ' +
                'confirmation popup to appear.'
        )
        alert = browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text
    except TimeoutException:
        print("no alert")
        return None
    except Exception as e:
        print("unhandled error", e)
        return None


link = "http://suninjuly.github.io/find_xpath_form"

try:
    browser = webdriver.Chrome()
    browser.maximize_window()
    browser.get(link)

    # TODO do something useful

    print(get_alert_text())
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()

