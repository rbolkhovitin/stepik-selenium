import time

from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.chrome.options import Options

try: 
    link = "http://suninjuly.github.io/registration1.html"  # works fine
    # link = "http://suninjuly.github.io/registration2.html"  # broken one
    options = Options()
    options.add_argument("--headless")
    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    browser.get(link)

    # Ваш код, который заполняет обязательные поля
    browser.find_element_by_css_selector(".first_block .first").send_keys("First Name")
    browser.find_element_by_css_selector(".first_block .second").send_keys("Second Name")
    browser.find_element_by_css_selector(".first_block .third").send_keys("Email")

    # Отправляем заполненную форму
    button = browser.find_element_by_css_selector("button.btn")
    button.click()

    # Проверяем, что смогли зарегистрироваться
    # ждем загрузки страницы
    time.sleep(1)

    # находим элемент, содержащий текст
    welcome_text_elt = browser.find_element_by_tag_name("h1")
    # записываем в переменную welcome_text текст из элемента welcome_text_elt
    welcome_text = welcome_text_elt.text

    # с помощью assert проверяем, что ожидаемый текст совпадает с текстом на странице сайта
    assert "Congratulations! You have successfully registered!" == welcome_text
except exceptions.WebDriverException as e:
    print("Script failed", e)
except Exception as e:
    print("Unexpected error", e)
else:
    print("Test passed")
finally:
    # ожидание чтобы визуально оценить результаты прохождения скрипта
    # закрываем браузер после всех манипуляций
    browser.quit()
