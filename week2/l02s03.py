import math
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
 

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))


def get_alert_text():
    try:
        WebDriverWait(browser, 3).until(
                EC.alert_is_present(),
                'Timed out waiting for PA creation ' +
                'confirmation popup to appear.'
        )
        alert = browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text
    except TimeoutException:
        print("no alert")
        return None
    except Exception as e:
        print("unhandled error", e)
        return None


link = "http://suninjuly.github.io/selects1.html"

try:
    options = Options()
    options.add_argument("--headless")

    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    browser.get(link)
    n1 = int(browser.find_element_by_id("num1").text)
    n2 = int(browser.find_element_by_id("num2").text)
    numbers_sum = str(n1 + n2)
    select = Select(browser.find_element_by_id("dropdown"))
    select.select_by_value(numbers_sum)
    browser.find_element_by_css_selector("button[type='submit']").click()
    print(get_alert_text())
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
