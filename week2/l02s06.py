import math
import time

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select
 

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))


def get_alert_text():
    try:
        WebDriverWait(browser, 3).until(
                EC.alert_is_present(),
                'Timed out waiting for PA creation ' +
                'confirmation popup to appear.'
        )
        alert = browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text
    except TimeoutException:
        print("no alert")
        return None
    except Exception as e:
        print("unhandled error", e)
        return None


link = "http://suninjuly.github.io/execute_script.html"

try:
    options = Options()
    options.add_argument("--headless")

    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    browser.get(link)
    
    x = browser.find_element_by_id("input_value").text
    browser.find_element_by_id("answer").send_keys(calc(x))
    
    browser.find_element_by_id("robotCheckbox").click()

    radio = browser.find_element_by_id("robotsRule")
    browser.execute_script("""
        return document.querySelector("#robotsRule").click()
    """)
    browser.execute_script("""
        return document.querySelector("button[type='submit']").click()
    """)
    
    print(get_alert_text())
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
