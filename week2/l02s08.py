import math
import time
import pathlib

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options


def get_alert_text():
    try:
        WebDriverWait(browser, 3).until(
                EC.alert_is_present(),
                'Timed out waiting for PA creation ' +
                'confirmation popup to appear.'
        )
        alert = browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text
    except TimeoutException:
        print("no alert")
        return None
    except Exception as e:
        print("unhandled error", e)
        return None


link = "http://suninjuly.github.io/file_input.html"

try:
    options = Options()
    options.add_argument("--headless")

    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    browser.get(link)
    
    browser.find_element_by_name("firstname").send_keys("hello")
    browser.find_element_by_name("lastname").send_keys("world")
    browser.find_element_by_name("email").send_keys("email@helloworld.com")
    p = pathlib.Path(__file__).parent / "../requirements.txt"
    browser.find_element_by_name("file").send_keys(p.resolve().absolute().as_posix())
    browser.find_element_by_css_selector("button[type='submit']").click()

    print(get_alert_text())
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
