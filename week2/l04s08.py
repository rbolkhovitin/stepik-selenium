import math
import time

from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

 
def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))


def get_alert_text():
    try:
        WebDriverWait(browser, 3).until(
                EC.alert_is_present(),
                'Timed out waiting for PA creation ' +
                'confirmation popup to appear.'
        )
        alert = browser.switch_to.alert
        text = alert.text
        alert.accept()
        return text
    except TimeoutException:
        print("no alert")
        return None
    except Exception as e:
        print("unhandled error", e)
        return None


link = "http://suninjuly.github.io/explicit_wait2.html"

try:
    options = Options()
    options.add_argument("--headless")

    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    browser.get(link)

    # book home
    WebDriverWait(browser, 20).until(
        EC.text_to_be_present_in_element((By.ID, "price"), "$100")
    )
    browser.find_element_by_id("book").click()
    
    # solve example
    x = browser.find_element_by_id("input_value").text
    y = calc(x)
    browser.find_element_by_id("answer").send_keys(y)
    browser.find_element_by_css_selector("button[type='submit']").click()
    print(get_alert_text())
finally:
    # закрываем браузер после всех манипуляций
    browser.quit()
