import math
import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

ids = ["236896", "236897", "236898", "236895", "236899", "236903", "236904", "236905",]

@pytest.fixture(scope="session")
def browser():
    options = Options()
    options.add_argument("--headless")
    browser = webdriver.Chrome(options=options)
    browser.maximize_window()
    yield browser
    browser.quit()

@pytest.fixture
def answer():
    return math.log(int(time.time()))

@pytest.fixture(scope="session")
def result():
    result = []
    yield result
    print("".join(result))


@pytest.mark.parametrize("id", ids, ids=ids)
def test_pages(id, browser: webdriver.Chrome, answer, result):
    browser.get(f"https://stepik.org/lesson/{id}/step/1")
    answer_selector = "div[data-type='string-quiz'] > textarea"
    # wait while textarea apeared
    textarea = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, answer_selector)
    ))
    textarea.send_keys(str(answer))

    # submit the answer
    browser.find_element_by_css_selector("button.submit-submission").click()

    # wait for result
    hint_selector = "pre.smart-hints__hint"
    hint = WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, hint_selector)
    ))
    wait = "Correct!"
    got = hint.text
    if got != wait:
        result.append(got)
    assert got == wait, f"wait {wait}, got {got}"
