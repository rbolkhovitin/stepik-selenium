import pytest

languages = ["ru", "de", "ua", "en-gb",]
labels = ["russian", "german", "ukrainian", "english", ]

@pytest.mark.parametrize("code", languages, ids=labels)
def test_guest_should_see_login_link(code):
    link = "http://selenium1py.pythonanywhere.com/{}/".format(code)
